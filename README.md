This project is intended to build a staging instance of Pan-Net DevOps Documentation.  
The documentation code is available in the following repository: https://gitlab.com/pan-net1/pan-net1.gitlab.io  
A staging instance of the website is available at the following URL: https://pan-net1.gitlab.io/pan-net1-stage  

## How to use this repository. 
Running pipeline in this repository will update GitLab pages staging instance. Production instance is not affected by this pipeline. 
To specify the production repository branch to be used to build staging instance use the `$REF_NAME` variable. The default value is `develop`
The pipeline may be triggered in two ways:

- automatically on an update of the `develop` branch of the production repository. In this case, the pipeline will pull the code from the `develop` branch of the production repository and run GitLab pages for it. 
- manually for the `develop` branch or any other branch of the production repository, specifying a different value for the `$REF_NAME` variable on run.  



